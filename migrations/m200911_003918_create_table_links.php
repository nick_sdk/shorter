<?php

use yii\db\Schema;
use yii\db\Migration;

/**
 * Class m200911_003918_create_table_links
 */
class m200911_003918_create_table_links extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('links', [
            'link_id'     => Schema::TYPE_PK,
            'link_hash'   => 'VARCHAR(32) NOT NULL',
            'link_url'    => 'VARCHAR(2048) NOT NULL',
            'link_clicks' => Schema::TYPE_INTEGER,
            'link_date'   => Schema::TYPE_DATETIME,
        ]);

        $this->createIndex(
            'idx__link_hash',
            'links',
            ['link_hash'],
            true
        );

        $this->createIndex(
            'idx__link_hash__link_date',
            'links',
            ['link_hash', 'link_date']
        );
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('links');
    }
}
