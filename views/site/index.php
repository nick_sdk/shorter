<?php

/* @var $this yii\web\View */
/* @var $form yii\bootstrap\ActiveForm */
/* @var $model app\models\Link */

use yii\helpers\Html;
use yii\bootstrap\ActiveForm;

$this->title = 'Link Shorter';
?>
<div class="site-index">
    <?= Html::beginForm(['/short'], 'post', ['id' => 'form-link']) ?>
    <?php $form = ActiveForm::begin([
        'id' => 'form-link',
        'class' => 'form-inline',
        'layout' => 'horizontal',
        'fieldConfig' => [
            'template' => '{label}'
                . '<div class="col-lg-5">{input}</div>'
                . '<div class="col-lg-2"><button type="submit" id="btn-link" class="btn btn-success">Go!</button></div>'
                . '<div class="col-lg-4">{error}</div>',
            'labelOptions' => ['class' => 'col-lg-1 control-label'],
        ],
    ]); ?>
    <?= $form->field($model, 'link_url')->textInput(['autofocus' => true, 'placeholder' => 'Insert URL']) ?>
    <?php ActiveForm::end(); ?>
</div>
