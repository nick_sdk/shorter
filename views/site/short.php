<?php

/* @var $this yii\web\View */
/* @var $model app\models\Link */

$this->title = 'Link Shorter';
?>
<div class="site-index">
    <div class="form-group" id="form-link">
        <div class="col-lg-2"></div>
        <div class="col-lg-8">
            <input type="text" class="form-control" id="input-link" value="<?= $model->getShortUrl() ?>" readonly="readonly">
        </div>
        <div class="col-lg-2"></div>
    </div>
</div>
<script>
    $('body').on('click', '#input-link', function () {
        $(this).select();
    });
</script>