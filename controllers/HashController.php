<?php

namespace app\controllers;

use app\models\Link;
use yii\web\Controller;

class HashController extends Controller
{

    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
        ];
    }

    public function actionIndex($hash)
    {
        if (empty($hash)) {
            $this->redirect('/')->send();
        }
        echo '<pre>';
        $link = Link::getByHash($hash);
        if (empty($link)) {
            $this->redirect('/')->send();
        } else {
            $link->link_clicks++;
            $link->save();
            $this->redirect($link->link_url)->send();
        }
    }
}
