<?php

namespace app\controllers;

use app\models\Link;
use Yii;
use yii\web\Controller;
use yii\web\Response;

class SiteController extends Controller
{

    /**
     * {@inheritdoc}
     */
    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
        ];
    }

    /**
     * Displays homepage.
     *
     * @return string
     */
    public function actionIndex()
    {
        $request = Yii::$app->getRequest();
        if (!empty($request->getPathInfo())) {
            $this->redirect('/')->send();
        }
        $model = new Link();
        return $this->render('index', [
            'model' => $model,
        ]);
    }

    /**
     * Short action.
     *
     * @return Response|string
     */
    public function actionShort()
    {
        $model = new Link();
        $post = Yii::$app->request->post();
        if (empty($post)) {
            $this->redirect('/')->send();
        }
        $model->load(Yii::$app->request->post());
        $oldModel = Link::getByHash($model->generateHash());
        if (!empty($oldModel)) {
            $model = $oldModel;
        }
        $model->save();
        if ($model->hasErrors()) {
            $errors = array_map(function ($error){
                return implode('<br>', $error);
            }, $model->getErrors());
            $message = implode('<br>', $errors);
            return $this->render('error', [
                'message' => $message,
                'name' => 'Error',
            ]);
        } else {
            return $this->render('short', [
                'model' => $model,
            ]);
        }
    }
}
