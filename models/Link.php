<?php

namespace app\models;

use yii\db\ActiveRecord;

/**
 * Link is the model of table "links"
 * @property int    $link_id;
 * @property string $link_hash;
 * @property string $link_url;
 * @property int    $link_clicks;
 * @property string $link_date;
 */
class Link extends ActiveRecord
{
    const SCENARIO_SAVE = 'save';

    const HASH_KEY_PREFIX = __METHOD__;

    const EXPIRE_TIME_MINUTES = 60 * 24; // 1 day

    public $primaryKey = 'link_id';

    /**
     * @return array the validation rules.
     */
    public function rules()
    {
        return [
            [['link_url'], 'required'],
            ['link_url', 'url'],
            ['link_url', 'generateHash'],
            [['link_hash', 'link_url', 'link_date'], 'required', 'on' => self::SCENARIO_SAVE],
        ];
    }

    public function beforeSave($insert)
    {
        $this->link_date = date('Y-m-d H:i:s');
        return parent::beforeSave($insert);
    }

    public function generateHash()
    {
        $this->link_hash = hash('crc32', self::HASH_KEY_PREFIX . '__' . $this->link_url);
        return $this->link_hash;
    }

    public function attributeLabels()
    {
        return [
            'link_hash'   => 'Hash',
            'link_url'    => 'Url',
            'link_clicks' => 'Clicks',
            'link_date'   => 'Date',
        ];
    }

    public static function tableName()
    {
        return 'links';
    }

    public function getShortUrl(): string
    {
        return \Yii::$app->getRequest()->hostInfo . '/h/' . $this->link_hash;
    }

    public static function getByHashNotExpired(string $hash)
    {
        $dateTime = new \DateTime();
        $dateTime->modify(sprintf('-%s minutes', self::EXPIRE_TIME_MINUTES));
        $dateTimeExpire = $dateTime->format('Y-m-d H:i:s');
        $command = \Yii::$app->getDb()->createCommand('SELECT link_id 
                FROM ' . self::tableName() . ' 
                WHERE 
                    link_hash = :link_hash 
                    AND link_date >= :link_date_expire
                ', [
            ':link_hash'        => $hash,
            ':link_date_expire' => $dateTimeExpire,
        ]);
        $linkId = $command->queryScalar();
        if ($linkId !== false) {
            return self::findOne(['link_id' => $linkId]);
        } else {
            return false;
        }
    }

    public static function getByHash(string $hash)
    {
        return Link::findOne(['link_hash' => $hash]);;
    }
}
