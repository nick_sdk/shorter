<?php

namespace app\models;

use Yii;
use yii\base\Model;

/**
 * LinkForm is the model behind the link form.
 *
 */
class LinkForm extends Model
{
    public $url;

    /**
     * @return array the validation rules.
     */
    public function rules()
    {
        return [
            [['url'], 'required'],
            ['url', 'url'],
        ];
    }
}
